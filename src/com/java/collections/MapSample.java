package com.java.collections;

import java.util.HashMap;
import java.util.Map;

public class MapSample {
	public static void main(String[] args) {
		Map<String, String> map = new HashMap<>();
		
		map.put("name", "Vikrant");
		map.put("id", "834729374");
		map.put("city", "Bangalore");
		map.put("city", "Chennai");
		map.put("tech", "Java");
		
		System.out.println(map.get("city"));
		map.remove("id");
		System.out.println(map.get("id"));
		
		System.out.println(map.keySet());
		System.out.println(map.values());
	}
}
