package com.java.collections;

import java.util.ArrayList;
import java.util.List;

public class ArrayListSample {

	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		
		list.add("Hello");
		list.add("Hello");
		list.add("Hello");
		list.add("Hi");
		list.add("Bye");
		list.add("Hi");
		list.add("Hi");
		list.add("Hi");
		list.add("Hello");
		list.add("Bye");

		for(String str: list){
			System.out.println(str);
		}
		
		list.remove(1);
		System.out.println("After removing element at 1st index");
		
		for(String str: list){
			System.out.println(str);
		}
		
		list.remove("Hi");
		System.out.println("After removing first occurences of Hi");
		
		for(String str: list){
			System.out.println(str);
		}
	}
}
