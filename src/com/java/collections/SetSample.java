package com.java.collections;

import java.util.HashSet;
import java.util.Set;

public class SetSample {
	
	public static void main(String[] args) {
		Set<String> set = new HashSet<>();
		
		set.add("Hi");
		set.add("Hi");
		set.add("Bye");
		set.add("Hi");
		set.add("Hi");
		set.add("Hello");
		set.add("Hello");
		set.add("Bye");
		set.add("Hello");
		set.add("Hello");
		set.add("Hi");
		set.add("Hello");
		set.add("Bye");
		
		for(String str: set){
			System.out.println(str);
		}
		
		set.remove("Hi");
		System.out.println("After removing occurences of Hi");
		
		for(String str: set){
			System.out.println(str);
		}
	}

}
