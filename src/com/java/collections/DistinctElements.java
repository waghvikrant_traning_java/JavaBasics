package com.java.collections;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class DistinctElements {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Set<Character> set = new HashSet<>();
		System.out.println(set.isEmpty());
		System.out.println("Enter a string to get its distinct elements : ");
		String input = in.nextLine();
		in.close();
		for (int i = 0; i < input.length(); i++) {
			set.add(input.charAt(i));
		}
		System.out.println(set.isEmpty());
		System.out.println("Distince alphabets are as following : ");
		System.out.println(set);
		System.out.println("Total number of distinct elements are : " + set.size());
	}
}
