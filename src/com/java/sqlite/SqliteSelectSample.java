package com.java.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SqliteSelectSample {
	public static void main(String[] args) {
		Connection c = null;
		Statement st = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:test.db");
			System.out.println("Successful connection to database");
			st = c.createStatement();
			String query = "select * from teacher";
			
			ResultSet rs = st.executeQuery(query);
			while(rs.next()){
				System.out.print("Name : " + rs.getString("name"));
				System.out.print(" ");
				System.out.print("Subject : " + rs.getString("subject"));
				System.out.print(" ");
				System.out.print("ID : " + rs.getString("id"));
				System.out.println(" ");
			}
			st.close();
			c.close();

		} catch (ClassNotFoundException e) {
			System.out.println("Driver not found");
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
