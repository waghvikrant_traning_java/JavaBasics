package com.java.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class SqliteInsertSample {
	public static void main(String[] args) {
		Connection c = null;
		Statement st = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:test.db");
			System.out.println("Successful connection to database");
			
		st = c.createStatement();
		String query = "insert into student values(\"Niveditha\",3, 12)";
		
		st.executeUpdate(query);
		st.close();
		c.close();
		
		} catch (ClassNotFoundException e) {
			System.out.println("Driver not found");
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
