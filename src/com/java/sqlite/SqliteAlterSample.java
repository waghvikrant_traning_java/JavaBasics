package com.java.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class SqliteAlterSample {
	public static void main(String[] args) {
		Connection c = null;
		Statement st = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:test.db");
			System.out.println("Successful connection to database");
			
		st = c.createStatement();
		String query = "alter table school_student rename to student";
		
		System.out.println("Changing name of table student to school_student");
		
		st.executeUpdate(query);
		
		System.out.println("Name of the table changed successfully");
		
		st.close();
		c.close();
		
		} catch (ClassNotFoundException e) {
			System.out.println("Driver not found");
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
