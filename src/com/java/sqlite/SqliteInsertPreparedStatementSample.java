package com.java.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SqliteInsertPreparedStatementSample {
	public static void main(String[] args) {
		Connection c = null;
		PreparedStatement st = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:test.db");
			System.out.println("Successful connection to database");
			String name = "Mike";
			String subject = "Biology";
			int id = 12;
			String query = "insert into teacher values(?,?,?)";
			
		st = c.prepareStatement(query);
		st.setString(1, name);
		st.setString(2, subject);
		st.setInt(3, id);
		
		st.execute();
		st.close();
		c.close();
		
		} catch (ClassNotFoundException e) {
			System.out.println("Driver not found");
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
