package com.java.basics.institute;

import com.java.basics.people.Student;
/*
 * Crating objects of classes in another classes in a different package
 */
public class College {

	public static void main(String[] args) {
		Student student1 = new Student();
		Student student2 = new Student(2, "Anil");
		
		student1.setName("Karthik");
		student1.setRoll(1);

		student1.display();
		student2.display();
	}

}
