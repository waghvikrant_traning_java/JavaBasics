package com.java.basics.people;
public class Student {
	long roll;
	String name;

	public Student() {
	}
	
	public Student(long roll, String name) {
		this.roll = roll;
		this.name = name;
	}

	public long getRoll() {
		return roll;
	}

	public void setRoll(long roll) {
		this.roll = roll;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void display(){
		show();
		System.out.println("Roll number : " + roll );
		System.out.println("Name : " +name);
	}
	
	void show(){
		System.out.println("I am a private method");
	}
}
