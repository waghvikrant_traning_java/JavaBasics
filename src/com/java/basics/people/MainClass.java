package com.java.basics.people;

/*
 * Crating objects of classes in another classes in a same package
 */
public class MainClass {

	public static void main(String[] args) {
		Student student = new Student();
			
		student.setName("Karthik");
		student.setRoll(1);

		student.display();
		
	System.out.println(student.name + " : " + student.roll);
	student.show();
	}

}
