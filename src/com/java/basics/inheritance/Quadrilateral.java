package com.java.basics.inheritance;

public abstract class Quadrilateral implements Shape{

	int length;
	int breadth;
	
	public Quadrilateral() {
		// Empty constructor
	}
	
	public Quadrilateral(int a, int b) {
		this.length = a;
		this.breadth = b;
	}
	
	public abstract double area();

	public abstract double perimeter();

	public int getLength() {
		return length;
	}

	public void setLength(int a) {
		this.length = a;
	}

	public int getBreadth() {
		return breadth;
	}

	public void setBreadth(int b) {
		this.breadth = b;
	}
}