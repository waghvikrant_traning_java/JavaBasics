package com.java.basics.inheritance;

public interface Shape {
	
	public double area();
	public double perimeter();

}
