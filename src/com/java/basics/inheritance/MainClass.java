package com.java.basics.inheritance;

public class MainClass {

	public static void main(String[] args) {

		Shape shapeSquare = new Square(4);
		System.out.println("Area of Square with Shape reference : " + shapeSquare.area());
		System.out.println("Perimeter of Square with Shape reference : " + shapeSquare.perimeter());
	
		Quadrilateral quadrilateralSquare = new Square(6);
		System.out.println("Area of Square with Quadrilateral reference : " + quadrilateralSquare.area());
		System.out.println("Perimeter of Square with Quadrilateral reference : " + quadrilateralSquare.perimeter());
	
		Square square = new Square();
		square.setLength(8);
		System.out.println("Area of Square with Square reference : " + square.area());
		System.out.println("Perimeter of Square with Square reference : " + square.perimeter());
		square.setBreadth(9);
		System.out.println("Area of Square with Square reference : " + square.area());
		System.out.println("Perimeter of Square with Square reference : " + square.perimeter());
		
		Shape shapeRectangle = new Rectangle(4, 5);
		System.out.println("Area of Rectangle with Shape reference : " + shapeRectangle.area());
		System.out.println("Perimeter of Rectangle with Shape reference : " + shapeRectangle.perimeter());
		
		Quadrilateral quadrilateralRectangle = new Rectangle(6, 7);
		System.out.println("Area of Rectangle with Quadrilateral reference : " + quadrilateralRectangle.area());
		System.out.println("Perimeter of Rectangle with Quadrilateral reference : " + quadrilateralRectangle.perimeter());
	
		Rectangle rectangle = new Rectangle();
		rectangle.setLength(3);
		rectangle.setBreadth(4);
		System.out.println("Area of Rectangle with Rectangle reference : " + rectangle.area());
		System.out.println("Perimeter of Rectangle with Rectangle reference : " + rectangle.perimeter());
	
		Shape shapeCircle = new Circle(7);
		System.out.println("Area of Circle with Shape reference : " + shapeCircle.area());
		System.out.println("Perimeter of Circle with Shape reference : " + shapeCircle.perimeter());
		
		Circle circle = new Circle(7);
		System.out.println("Area of Circle with Circle reference : " + circle.area());
		System.out.println("Perimeter of Circle with Circle reference : " + circle.perimeter());
	}
}
