package com.java.basics.inheritance;

public class Rectangle extends Quadrilateral {
	
	public Rectangle() {
		super();
	}

	public Rectangle(int a, int b) {
		super(a, b);
	}
	
	@Override
	public double area() {
		double area = length*breadth;
		return area;
	}

	@Override
	public double perimeter() {
		double perimeter = 2 * (length + breadth);
		return perimeter;
	}

}
