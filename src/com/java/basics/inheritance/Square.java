package com.java.basics.inheritance;

public class Square extends Quadrilateral {
	
	public Square() {
		super();
	}
	
	public Square(int a) {
		super(a,a);
	}
	
	@Override
	public void setBreadth(int b) {
		setLength(b);
	}

	@Override
	public double area() {
		return length*length;
	}

	@Override
	public double perimeter() {
		return 4*length;
	}
}
