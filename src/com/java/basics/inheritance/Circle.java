package com.java.basics.inheritance;

public class Circle implements Shape {
	
	int radius;
	
	public Circle() {
	}
	
	public Circle(int r) {
		this.radius = r;
	}

	public int getRadius() {		
		return radius;
	}

	public void setRadius(int r) {
		this.radius = r;
	}

	@Override
	public double area() {
		return Math.PI*radius*radius;
	}


	public double perimeter(){
		return Math.PI*radius*2;
	}

}
